# Intro #
A script to quickly bootstrap a Golang API server.

run 

```
/bin/bash -c "$(curl -fsSL https://gitlab.com/brokkrs4ge/gostrapper/-/raw/0.1.1/api-server/start-go.sh)"
```

the default datastore here is `postgres`, but other flavors can be easily plugged in as well

the package `dbx` contains barebone functions, which you should be able to fill in with your db connection inteface