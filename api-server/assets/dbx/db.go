package dbx

type Dbx interface {
}

type Txx interface{}

func MustNew(dsn string, opts ...optFn) Dbx {
	db, err := New(dsn)
	if err != nil {
		panic(err)
	}
	return db
}

type options struct{}

type optFn func(o *options)

func New(dsn string, opts ...optFn) (Dbx, error) {
	var o options
	for _, fn := range opts {
		fn(&o)
	}
	var iface struct{}

	// implement db connection here. since db package selection varies by project/team, you'll have to do some impl here
	//  suggest bun (with ORM), or sqlx if you're more into barebone/lightweight
	return iface, nil
}
