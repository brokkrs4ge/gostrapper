package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

var (
	port = ":8000"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/health", Health)
	http.ListenAndServe(port, r)
}

func Health(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"status":"ok"}`))

}
